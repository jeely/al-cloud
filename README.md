## 平台简介


jar包依赖问题 解决方案 参考下面文档
http://123.56.93.7:8080/web/#/9?page_id=1114
~~~
com.ruoyi     
├── ruoyi-ui              // 前端框架 [80]
├── ruoyi-gateway         // 网关模块 [8080]
├── ruoyi-auth            // 认证中心 [9200]
├── ruoyi-api             // 接口模块
│       └── ruoyi-api-system                          // 系统接口
├── ruoyi-common          // 通用模块
│       └── ruoyi-common-core                         // 核心模块
│       └── ruoyi-common-datascope                    // 权限范围
│       └── ruoyi-common-datasource                   // 多数据源
│       └── ruoyi-common-log                          // 日志记录
│       └── ruoyi-common-redis                        // 缓存服务
│       └── ruoyi-common-security                     // 安全模块
│       └── ruoyi-common-swagger                      // 系统接口
├── ruoyi-modules         // 业务模块
│       └── ruoyi-system                              // 系统模块 [9201]
│       └── ruoyi-gen                                 // 代码生成 [9202]
│       └── ruoyi-job                                 // 定时任务 [9203]
│       └── ruoyi-file                                // 文件服务 [9300]
├── ruoyi-visual          // 图形化管理模块
│       └── ruoyi-visual-monitor                      // 监控中心 [9100]
├──pom.xml                // 公共依赖
~~~

## 架构图

<img src="https://oscimg.oschina.net/oscnet/up-63c1c1dd2dc2b91d498164d9ee33682a32a.png"/>
项目需要的 公共jar包 代码
https://gitee.com/bujiDemon/commonjar

## 若依微服务交流群

QQ群： [![加入QQ群](https://img.shields.io/badge/已满-42799195-blue.svg)](https://jq.qq.com/?_wv=1027&k=yqInfq0S) [![加入QQ群](https://img.shields.io/badge/已满-170157040-blue.svg)](https://jq.qq.com/?_wv=1027&k=Oy1mb3p8) [![加入QQ群](https://img.shields.io/badge/130643120-blue.svg)](https://jq.qq.com/?_wv=1027&k=rvxkJtXK) 点击按钮入群。

扩展内容
http://123.56.93.7:8080/web/#/9?page_id=871
# 1.集成maven-assembly-plugin插件  一键打包 更方便系统的迁移部署

![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/6036fe8e92840.png)

# 2.优化接口文档工具springfox-swagger2 更美观
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/6036ff81a0120.png)

# 3.集成mybaits-plus 精简代码
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/6037009eb8d83.png)

# 4.实现自动更新 插入人 插入时间  更新人  更新时间
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/6037018251e07.png)

# 5.翻译注解的实现  通过设置注解 对应字典编码 将翻译后的值 设置到对应字段
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/603702b0acb64.png)

# 6.优化微服务代码生成模块 多数据源切换问题
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/603703c3e6eff.png)
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/603704b7e1ce3.png)
# 7.优化微服务 代码模板在线配置
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/603704dec7da4.png)
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/603704f88dc78.png)
# 8.错误码组件
优雅的抛出运行时错误异常，并由前端捕获
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/603721103faf2.png)
![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/603721185eb79.png)


扩展项目启动方法：
http://123.56.93.7:8080/web/#/page/edit/9/872
    微服务本质为通过生态，实现服务和数据的综合治理。
    新人学习将面临众多的，组件的安装学习，对于开发来说费力不讨好。会极大打击学习兴趣。
    建议学习docker 采用容器化部署。微服务的基础环境路径在build-cloud/dockercompose/docker-compose-env.yml(未完整 是有执行顺序的)
  # 1.启动命令
    docker-compose up -d
    启动的服务有 mysql redis nacos sentinel minio
  # 2.初始化数据
  像mysql中 依次建库 并执行sql脚本
  ![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/60370d6a1c5d2.png)
  # 3.替换项目配置
  全局替换 nacos 和 sentinel的配置地址
  ![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/60370dba00bc2.png)
  # 4.修改对应nacos的配置
  修改对应的 redis 和 mysql minio的配置
  ![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/60370df59e3d0.png)
  # 5.本地依次启动项目 验证功能可用
  
  # 6.部署配置
  ## （1）前端打包
  ![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/60370e5542e0c.png)
  将打包后的dist文件拷贝到 build-cloud/vue/prodist 路径下
  ## （2）修改前端跨域
  修改build-cloud/vue/default.conf文件中的
  ![](http://123.56.93.7:8080/server/../Public/Uploads/2021-02-25/60370eab12f27.png)
  为gateway的地址
  ## （3）项目打包 触发maven-assembly-plugin
  打包好后 进入D:\work\rycloud240\build-cloud\target\build-assembly 拷贝到linux服务器 执行
    docker-compose up -d 就可一键启动 所有前后台
