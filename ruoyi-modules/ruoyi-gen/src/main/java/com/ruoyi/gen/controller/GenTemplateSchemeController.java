package com.ruoyi.gen.controller;

import com.common.zrd.json.CommonJsonResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ruoyi.gen.domain.GenTemplateScheme;
import com.ruoyi.gen.service.IGenTemplateSchemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 代码生成模板组管理Controller
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@RestController
@RequestMapping("/scheme" )
public class GenTemplateSchemeController extends BaseController {
    @Autowired
    private IGenTemplateSchemeService genTemplateSchemeService;

    /**
     * 查询代码生成模板组管理列表
     */
    @PreAuthorize(hasPermi = "gen:scheme:list" )
    @GetMapping("/list" )
    public TableDataInfo list(GenTemplateScheme genTemplateScheme) {
        startPage();
        List<GenTemplateScheme> list = genTemplateSchemeService.selectGenTemplateSchemeList(genTemplateScheme);
        return getDataTable(list);
    }

    /**
     * 查询代码生成模板组管理列表
     */
    @PreAuthorize(hasPermi = "gen:scheme:list" )
    @GetMapping("/getAll" )
    public CommonJsonResult getAll(GenTemplateScheme genTemplateScheme) {
        List<GenTemplateScheme> list = genTemplateSchemeService.selectGenTemplateSchemeList(genTemplateScheme);
        return CommonJsonResult.of(list);
    }

    /**
     * 导出代码生成模板组管理列表
     */
    @PreAuthorize(hasPermi = "gen:scheme:export" )
    @Log(title = "代码生成模板组管理" , businessType = BusinessType.EXPORT)
    @PostMapping("/export" )
    public void export(HttpServletResponse response, GenTemplateScheme genTemplateScheme) throws IOException {
        List<GenTemplateScheme> list = genTemplateSchemeService.selectGenTemplateSchemeList(genTemplateScheme);
        ExcelUtil<GenTemplateScheme> util = new ExcelUtil<>(GenTemplateScheme.class);
        util.exportExcel(response, list, "scheme" );
    }

    /**
     * 获取代码生成模板组管理详细信息
     */
    @PreAuthorize(hasPermi = "gen:scheme:query" )
    @GetMapping(value = "/{id}" )
    public AjaxResult getInfo(@PathVariable("id" ) Long id) {
        return AjaxResult.success(genTemplateSchemeService.getById(id));
    }

    /**
     * 新增代码生成模板组管理
     */
    @PreAuthorize(hasPermi = "gen:scheme:add" )
    @Log(title = "代码生成模板组管理" , businessType = BusinessType.INSERT)
    @PostMapping
    public CommonJsonResult add(@RequestBody GenTemplateScheme genTemplateScheme) {
        genTemplateSchemeService.save(genTemplateScheme);
        return CommonJsonResult.of(genTemplateScheme);
    }

    /**
     * 修改代码生成模板组管理
     */
    @PreAuthorize(hasPermi = "gen:scheme:edit" )
    @Log(title = "代码生成模板组管理" , businessType = BusinessType.UPDATE)
    @PutMapping
    public CommonJsonResult edit(@RequestBody GenTemplateScheme genTemplateScheme) {
        genTemplateSchemeService.updateById(genTemplateScheme);
        return CommonJsonResult.of(genTemplateScheme);
    }

    /**
     * 删除代码生成模板组管理
     */
    @PreAuthorize(hasPermi = "gen:scheme:remove" )
    @Log(title = "代码生成模板组管理" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}" )
    public CommonJsonResult remove(@PathVariable Long[] ids) {

        genTemplateSchemeService.removeByIds(Arrays.asList(ids));
        return CommonJsonResult.empty();
    }
}
